//
// Created by ASUS on 05.02.2021.
//

#include <stdio.h>
#include "image.h"
#include "bmp_reader.h"
#include "printer.h"

void usage() {
    fprintf(stderr, "Usage: ./rotate_image BMP_FILE_NAME\n");
}

int main(int argc, char** argv) {
    if (argc != 2) usage();
    if (argc < 2) fprintf(stderr, "Not enough arguments \n" );
    if (argc > 2) fprintf(stderr, "Too many arguments \n" );

    struct image img = img_create_empty();

    FILE* in = fopen( argv[1], "rb" );

    if (from_bmp(in, &img) != READ_OK) {
        printf("Error while reading\n");
        return 1;
    }

    fclose(in);

    struct image rotated_img = img_rotate(img);
    img_destroy(&img);

    FILE* out = fopen( argv[1], "wb" );

    if (to_bmp(out, &rotated_img) != WRITE_OK) {
        printf("Error while writing\n");
        return 1;
    }

    fclose(out);

    img_destroy(&rotated_img);

    return 0;
}