//
// Created by ASUS on 05.02.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include <stdint.h>
#include <inttypes.h>

struct image {
    uint32_t width, height;
    struct pixel* data;
};

struct pixel { uint8_t b, g, r; };

struct image img_rotate(struct image const source);

struct image img_create_empty();
struct image img_create(uint32_t width, uint32_t height);
void img_destroy(struct image* img);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
