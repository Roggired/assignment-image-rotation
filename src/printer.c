//
// Created by ASUS on 06.02.2021.
//

#include "printer.h"

#include <stdio.h>
#include <inttypes.h>
#include "image.h"
#include "bmp_reader.h"

#define PRI_SPECIFIER(e) (_Generic( (e), uint16_t : "%" PRIu16, uint32_t: "%" PRIu32, default: "NOT IMPLEMENTED" ))

#define PRINT_FIELD( t, name ) \
    fprintf( f, "%-17s: ",  # name ); \
    fprintf( f, PRI_SPECIFIER( header-> name ) , header-> name );\
    fprintf( f, "\n");


void bmp_header_print( struct bmp_header const* header, FILE* f ) {
    FOR_BMP_HEADER( PRINT_FIELD )
}

void pixel_print(struct pixel const pixel, FILE* f) {
    fprintf(f, "%3" PRIu8 " %3" PRIu8 " %3" PRIu8, pixel.b, pixel.g, pixel.r);
}

void img_print(struct image const* img, FILE* f) {
    fprintf(f, "Width: %" PRIu32, img->width);
    fprintf(f, "\n");
    fprintf(f, "Height: %" PRIu32, img->height);
    fprintf(f, "\n");

    for (uint32_t i = 0; i < img->height; i++) {
        for (uint32_t j = 0; j < img->width; j++) {
            pixel_print(img->data[i * img->width + j], f);
            fprintf(f, "|");
        }

        fprintf(f, "\n");
    }
}

