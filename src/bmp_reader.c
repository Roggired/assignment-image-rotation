//
// Created by ASUS on 05.02.2021.
//

#include "bmp_reader.h"
#include <stdbool.h>
#include <inttypes.h>
#include "image.h"
#include "printer.h"


static
size_t header_get_junk_bytes(struct bmp_header const* header) {
    if ((header->biWidth * 3) % 4 == 0) {
        return 0;
    }

    return 4 - (header->biWidth * 3) % 4;
}

static
uint16_t img_get_junk_bytes_amount(struct image const* img) {
    if ((img->width * 3) % 4 == 0) {
        return 0;
    }

    return 4 - (img->width * 3) % 4;
}

static
uint32_t img_get_biSizeImage(struct image const* img) {
    return (img->width * 3 + img_get_junk_bytes_amount(img)) * img->height;
}

static
uint32_t img_get_bfileSize(struct image const* img) {
    return img_get_biSizeImage(img) + HEADER_BOFF_SIZE;
}

static
struct bmp_header create_header(struct image const* img) {
    return (struct bmp_header)
            {
                    HEADER_BF_TYPE,
                    img_get_bfileSize(img),
                    HEADER_BF_RESERVED,
                    HEADER_BOFF_SIZE,
                    HEADER_BI_SIZE,
                    img->width,
                    img->height,
                    HEADER_BI_PLANES,
                    HEADER_BIT_COUNT,
                    HEADER_BI_COMPRESSION,
                    img_get_biSizeImage(img),
                    HEADER_BI_X_PELS_PER_METER,
                    HEADER_BI_Y_PELS_PER_METER,
                    HEADER_BI_CLR_USED,
                    HEADER_BI_CLR_IMPORTANT
            };
}

static
bool read_header(FILE* in, struct bmp_header* header) {
    return fread( header, sizeof( struct bmp_header ), 1, in );
}

static
bool read_pixel(struct pixel* p, FILE* in) {
    return fread( p, sizeof(struct pixel), 1, in );
}

static
bool read_data(FILE* in, struct pixel* data, struct bmp_header* header) {
    const size_t row_size = header->biWidth;
    const uint16_t junk_bytes_amount = header_get_junk_bytes(header);

    char junk_yard[100];

    for (uint32_t i = 0; i < header->biHeight * header->biWidth; i++) {
        if (i % row_size == 0 && i != 0 && junk_bytes_amount != 0) {
            if (!fread( &junk_yard, junk_bytes_amount, 1, in )) {
                return false;
            }
        }

        if (!read_pixel(data + i, in)) {
            return false;
        }
    }

    return true;
}

static
bool write_header(FILE* out, struct bmp_header* header) {
    return fwrite( header, sizeof(struct bmp_header), 1, out );
}

static
bool pixel_write(struct pixel* p, FILE* out) {
    return fwrite(p, sizeof(struct pixel), 1, out);
}

static
bool write_data(FILE* out, struct pixel* data, struct bmp_header* header) {
    const size_t row_size = header->biWidth;
    const size_t junk_bytes_amount = header_get_junk_bytes(header);

    char junk = 1;

    for (uint32_t i = 0; i < header->biHeight * header->biWidth; i++) {
        if (i % row_size == 0 && i != 0 && junk_bytes_amount != 0) {
            if (!fwrite( &junk, sizeof(char), junk_bytes_amount, out )) {
                return false;
            }
        }

        if (!pixel_write(data + i, out)) {
            return false;
        }
    }

    return true;
}

enum read_status from_bmp( FILE* in, struct image* img ) {
    struct bmp_header header = {0};

    if (!read_header(in, &header)) {
        return READ_INVALID_HEADER;
    }

    *img = img_create(header.biWidth, header.biHeight);

    if (!read_data(in, img->data, &header)) {
        return READ_IMAGE_FAIL;
    }

    return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img ) {
    struct bmp_header header = create_header(img);

    if (!write_header(out, &header)) {
        return WRITE_ERROR;
    }

    if (!write_data(out, img->data, &header)) {
        return WRITE_ERROR;
    }

    char last_byte = 0;
    fwrite(&last_byte, sizeof(char), 1, out);
    fwrite(&last_byte, sizeof(char), 1, out);

    return WRITE_OK;
}