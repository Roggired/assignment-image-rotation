CFLAGS=--std=c18 -Wall -pedantic -Isrc/ -ggdb -Wextra -Werror -DDEBUG
CC=gcc


TARGET ?= rotate_image
SRC_DIRS ?= src/
OBJ_DIR ?= obj/

OBJS := $(shell find $(OBJ_DIR) -name "*.o")

%.o: %.c
	$(CC) -c $(CFLAGS) $< -o $@

$(TARGET): $(OBJS)
	$(CC) $(OBJS) -o $@

.PHONY: clean
clean:
	$(RM) $(TARGET) $(OBJS) $(DEPS)
