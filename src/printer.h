//
// Created by ASUS on 06.02.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_PRINTER_H
#define ASSIGNMENT_IMAGE_ROTATION_PRINTER_H

#include <stdio.h>
#include <inttypes.h>
#include "bmp_reader.h"
#include "image.h"

#define FOR_BMP_HEADER( FOR_FIELD ) \
        FOR_FIELD( uint16_t,bfType)\
        FOR_FIELD( uint32_t,bfileSize)\
        FOR_FIELD( uint32_t,bfReserved)\
        FOR_FIELD( uint32_t,bOffBits)\
        FOR_FIELD( uint32_t,biSize)\
        FOR_FIELD( uint32_t,biWidth)\
        FOR_FIELD( uint32_t,biHeight)\
        FOR_FIELD( uint16_t,biPlanes)\
        FOR_FIELD( uint16_t,biBitCount)\
        FOR_FIELD( uint32_t,biCompression)\
        FOR_FIELD( uint32_t,biSizeImage)\
        FOR_FIELD( uint32_t,biXPelsPerMeter)\
        FOR_FIELD( uint32_t,biYPelsPerMeter)\
        FOR_FIELD( uint32_t,biClrUsed)\
        FOR_FIELD( uint32_t,biClrImportant)

#define DECLARE_FIELD( t, n ) t n ;

void bmp_header_print( struct bmp_header const* header, FILE* f );
void pixel_print(struct pixel const pixel, FILE* f);
void img_print(struct image const* img, FILE* f);

#endif //ASSIGNMENT_IMAGE_ROTATION_PRINTER_H
