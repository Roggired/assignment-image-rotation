//
// Created by ASUS on 05.02.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_READER_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_READER_H

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "image.h"

static const uint16_t HEADER_BF_TYPE = 19778;
static const uint32_t HEADER_BF_RESERVED = 0;
static const uint32_t HEADER_EXTRA_OFFSET = 14;
static const uint32_t HEADER_BI_SIZE = 40;
static const uint32_t HEADER_BOFF_SIZE = HEADER_BI_SIZE + HEADER_EXTRA_OFFSET;
static const uint32_t HEADER_BIT_COUNT = 24;
static const uint16_t HEADER_BI_PLANES = 1;
static const uint32_t HEADER_BI_COMPRESSION = 0;
static const uint32_t HEADER_BI_X_PELS_PER_METER = 5906;
static const uint32_t HEADER_BI_Y_PELS_PER_METER = 5906;
static const uint32_t HEADER_BI_CLR_USED = 0;
static const uint32_t HEADER_BI_CLR_IMPORTANT = 0;

struct __attribute__((packed)) bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};


/*  deserializer   */
enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_IMAGE_FAIL
    /* коды других ошибок  */
};

enum read_status from_bmp( FILE* in, struct image* img );

/*  serializer   */
enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
    /* коды других ошибок  */
};

enum write_status to_bmp( FILE* out, struct image const* img );


#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_READER_H
