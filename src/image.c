//
// Created by ASUS on 05.02.2021.
//

#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>
#include "image.h"

struct image img_create_empty() {
    struct pixel data[10];
    return  (struct image){0, 0, data};
}

struct image img_create(uint32_t width, uint32_t height) {
    struct pixel* data = malloc(sizeof(struct pixel) * width * height);
    return (struct image){width, height, data};
}

void img_destroy(struct image* img) {
    free(img->data);
}

struct image img_rotate(struct image const source) {
    struct image result = img_create(source.height, source.width);

    for (uint32_t i = 0; i < source.height; i++) {
        for (int64_t j = source.width - 1; j >= 0; j--) {
            result.data[i + (source.width - 1 - j) * source.height] = source.data[j + i * source.width];
        }
    }

//    result.width = source.height;
//    result.height = source.width;

    return result;
}

//1 2 3 4 5   0  1  2  3  4
//6 7 8 9 1   5  6  7  8  9
//2 3 4 5 6   10 11 12 13 14
//
//-->
//
//5 1 6       0   1   2
//4 9 5       3   4   5
//3 8 4       6   7   8
//2 7 3       9   10  11
//1 6 2       12  13  14



